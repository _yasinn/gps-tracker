package com.griffith.gpstracker.util

import android.location.Location
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object GPXWriter {
    private val TAG = GPXWriter::class.java.name
    // Creating path for gpx file
    fun writePath(file: File?, n: String, points: List<Location>) {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        var segments = ""

        for (l in points) {
            segments += GPXHelper.fineSegment(l.latitude, l.longitude, df.format(Date(l.time)))
        }
        val fileData = GPXHelper.fileHeader() + GPXHelper.fileName(n) + segments + GPXHelper.fileFooter();

        try {
            val fileOutPutStream = FileOutputStream(file)
            fileOutPutStream.write(fileData.toByteArray())
            fileOutPutStream.close()
            Log.i(TAG, "Saved " + points.size + " points.")
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e(TAG, "Error While Writing Path!", e)
        }
    }
}