package com.griffith.gpstracker.util

object GPXHelper {
    fun fileName(str: String): String{
        val name = "<name>$str</name><trkseg>\n";
        return name
    }

    fun fileHeader(): String {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" " +
                "?><gpx xmlns=\"http://www.topografix.com/GPX/1/1\" " +
                "creator=\"MapSource 6.15.5\" version=\"1.1\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  " +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 " +
                "http://www.topografix.com/GPX/1/1/gpx.xsd\"><trk>\n"
    }

    fun fineSegment(longitude: Double, latitude: Double, timeStamp: String): String {
        return "<trkpt lat=\"$longitude\" lon=\"$latitude\"><time>$timeStamp</time></trkpt>\n"
    }

    fun fileFooter(): String {
        return "</trkseg></trk></gpx>"
    }
}