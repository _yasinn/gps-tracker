package com.griffith.gpstracker.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.griffith.gpstracker.model.LocationList

class GPSGraphView : View{
    private var _locationDataSet: LocationList? = null
    private var _widthHeight = 0f
    private lateinit var _kmhLinesPoints: FloatArray
    private lateinit var _speedGraphPoints: FloatArray
    private lateinit var _averageLinePoints: FloatArray

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr ) {
        init()
    }

    companion object {
        private const val DEFAULT_BLACK_COLOR = Color.BLACK
        private const val DEFAULT_WHITE_COLOR = Color.WHITE
        private const val DEFAULT_RED_COLOR = Color.RED
        private const val DEFAULT_GREEN_COLOR = Color.GREEN
        private val DEFAULT_STROKE_STYLE = Paint.Style.STROKE
        private const val DEFAULT_STROKE_WIDTH = 1.0f
    }

    /**
     * Defining Paint
     * */
    private val paintLineWhite = Paint().apply {
        color = DEFAULT_WHITE_COLOR
    }

    private val paintLineBlack = Paint().apply {
        color = DEFAULT_BLACK_COLOR
    }

    private val paintLineGreen = Paint().apply {
        color = DEFAULT_GREEN_COLOR
        style = DEFAULT_STROKE_STYLE
        strokeWidth = DEFAULT_STROKE_WIDTH
    }

    private val paintLineRed = Paint().apply {
        color = DEFAULT_RED_COLOR
    }

    private fun init (){
        // 6 lines * 4
        _kmhLinesPoints = FloatArray(24)
        _speedGraphPoints = FloatArray(0)
        _averageLinePoints = FloatArray(4)
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setPadding(0, 0, 0, 0)

        // force the view to be square in size
        val height = measuredHeight
        val width = measuredWidth
        _widthHeight = if (height > width) {
            width.toFloat()
        } else {
            height.toFloat()
        }
        setMeasuredDimension(_widthHeight.toInt(), _widthHeight.toInt())
        val cellWidthHeight = _widthHeight / 6
        var i = 0
        while (i < _kmhLinesPoints.size) {

            // first point x, y
            _kmhLinesPoints[i] = 0f
            _kmhLinesPoints[i + 1] = cellWidthHeight * (i / 4)
            // second point x, y
            _kmhLinesPoints[i + 2] = _widthHeight
            _kmhLinesPoints[i + 3] = cellWidthHeight * (i / 4)
            i += 4
        }
        onLocationDataChanged()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawRect(0f, 0f, _widthHeight, _widthHeight, paintLineBlack!!)
        canvas.drawLines(_kmhLinesPoints, paintLineWhite!!)
        if (_speedGraphPoints.isNotEmpty()) {
            canvas.drawLines(_speedGraphPoints, paintLineGreen!!)
            canvas.drawLines(_averageLinePoints, paintLineRed!!)
        }
    }

    // When new point added to list after calling this method for update graph
    private fun onLocationDataChanged() {
        if (_locationDataSet!!.customGpsLocations.size == 0) {
            return
        }
        _speedGraphPoints = FloatArray(_locationDataSet!!.customGpsLocations.size * 4 - 4)
        val marginBetweenPoints: Float = if (_locationDataSet!!.customGpsLocations.size > 1) {
            _widthHeight / (_locationDataSet!!.customGpsLocations.size - 1)
        } else {
            _widthHeight / _locationDataSet!!.customGpsLocations.size
        }
        var i = 0
        while (i < _speedGraphPoints.size) {

            // first point x, y
            _speedGraphPoints[i] = marginBetweenPoints * (i / 4)
            _speedGraphPoints[i + 1] =
                _widthHeight / 60 * (60 - _locationDataSet!!.getCurrentSpeed(i / 4))
            // second point x, y
            _speedGraphPoints[i + 2] = marginBetweenPoints * ((i + 4) / 4)
            _speedGraphPoints[i + 3] =
                _widthHeight / 60 * (60 - _locationDataSet!!.getCurrentSpeed((i + 4) / 4))
            i += 4
        }

        // average line points
        // startX, startY
        _averageLinePoints[0] = 0f
        _averageLinePoints[1] = _widthHeight / 60 * (60 - _locationDataSet!!.averageSpeed)
        // endX, endY
        _averageLinePoints[2] = _widthHeight
        _averageLinePoints[3] = _widthHeight / 60 * (60 - _locationDataSet!!.averageSpeed)

        invalidate()
    }

    fun setData(locationList: LocationList?) {
        _locationDataSet = locationList
    }
}