package com.griffith.gpstracker.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.griffith.gpstracker.R

class SantaRunning : SurfaceView, Runnable {
    private var gameThread: Thread? = null
    private val ourHolder: SurfaceHolder = holder

    private var playing = false
    private lateinit var canvas: Canvas
    private var bitmapRunningMan: Bitmap
    private var isMoving = true
    private val runSpeedPerSecond = 400f
    private var manXPos = 10f
    private var manYPos = 10f
    private val frameWidth = 200
    private val frameHeight = 274
    private val frameCount = 8
    private var currentFrame = 0
    private var fps: Long = 0
    private var timeThisFrame: Long = 0
    private var lastFrameChangeTime: Long = 0
    private val frameLengthInMillisecond = 50
    private val frameToDraw = Rect(0, 0, frameWidth, frameHeight)
    private val whereToDraw =
        RectF(manXPos, manYPos, manXPos + frameWidth, frameHeight.toFloat())

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr )

    // Start santa run
    override fun run() {
        while (playing) {
            val startFrameTime = System.currentTimeMillis()
            update()
            draw()
            timeThisFrame = System.currentTimeMillis() - startFrameTime
            if (timeThisFrame >= 1) {
                fps = 1000 / timeThisFrame
            }
        }
    }

    // Updating santa movements
    private fun update() {
        if (isMoving) {
            manXPos += runSpeedPerSecond / fps
            if (manXPos > width) {
                manYPos += frameHeight
                manXPos = 10f
            }
            if (manYPos + frameHeight > height) {
                manYPos = 10f
            }
        }
    }

    // Each step for santa sliding window on png
    private fun manageCurrentFrame() {
        val time = System.currentTimeMillis()
        if (isMoving) {
            if (time > lastFrameChangeTime + frameLengthInMillisecond) {
                lastFrameChangeTime = time
                currentFrame++
                if (currentFrame >= frameCount) {
                    currentFrame = 0
                }
            }
        }
        frameToDraw.left = currentFrame * frameWidth
        frameToDraw.right = frameToDraw.left + frameWidth
    }

    // draw santa for each step
    private fun draw() {
        if (ourHolder.surface.isValid) {
            canvas = ourHolder.lockCanvas()
            canvas.drawColor(Color.WHITE)
            whereToDraw.set(
                manXPos,
                manYPos,
                (manXPos.toInt() + frameWidth).toFloat(),
                (manYPos.toInt() + frameHeight).toFloat()
            )
            manageCurrentFrame()
            canvas.drawBitmap(bitmapRunningMan, frameToDraw, whereToDraw, null)
            ourHolder.unlockCanvasAndPost(canvas)
        }
    }

    // Stop santa
    fun pause() {
        playing = false
        try {
            gameThread!!.join()
        } catch (e: InterruptedException) {
            Log.e("ERR", "Joining Thread")
        }
    }

    // Resume santa
    fun resume() {
        playing = true
        gameThread = Thread(this)
        gameThread!!.start()
    }

    // If you are touching on the screen when santa running then santa will be stop
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> isMoving = !isMoving
        }
        return true
    }

    // Initializing santa from png
    init {
        bitmapRunningMan = BitmapFactory.decodeResource(resources, R.drawable.ic_santa_exercise)
        bitmapRunningMan = Bitmap.createScaledBitmap(
            bitmapRunningMan,
            frameWidth * frameCount,
            frameHeight,
            false
        )
    }

}