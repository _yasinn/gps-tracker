package com.griffith.gpstracker.model

import android.location.Location
import com.griffith.gpstracker.util.CalculationUtil
import java.util.ArrayList
import kotlin.math.abs

class LocationList(listSize: Int) {
    var customGpsLocations: MutableList<CustomGPSLocation> = ArrayList()
    private val maxListSize: Int = listSize
    var averageSpeed = 0f

    /**
     * Location adding to the list
     * */
    fun addLocation(location: Location) {
        if (customGpsLocations.size >= maxListSize) {
            customGpsLocations.removeAt(0)
        }
        if (customGpsLocations.size == 0) {
            val customLocation = CustomGPSLocation(location)
            customGpsLocations.add(customLocation)
        } else {
            val customLocation =
                CustomGPSLocation(location, customGpsLocations[customGpsLocations.size - 1].location)
            customGpsLocations.add(customLocation)
        }
        averageSpeed = calculateAverageSpeed()
    }

    /**
     * Calculating average includes each indices
     */
    private fun calculateAverageSpeed(): Float {
        var overall = 0f
        for (i in customGpsLocations.indices) {
            overall += customGpsLocations[i].current_speed
        }
        return overall / customGpsLocations.size
    }

    /**
     * If need find current speed
     * */
    fun getCurrentSpeed(at: Int): Float {
        return customGpsLocations[at].current_speed
    }

    /**
     * Calculating total distance
     * */
    fun calculateTotalDistance(): Double {
        var distance = 0.0
        if(customGpsLocations.isNotEmpty()){
            val firstLocation = customGpsLocations[0]
            val lastLocation = customGpsLocations[customGpsLocations.size - 1]

            distance =  CalculationUtil.distance(firstLocation.location.latitude, firstLocation.location.longitude,
                lastLocation.location.latitude, lastLocation.location.longitude)
        }

        return distance
    }

    /**
     * Finding Max Altitude
     * */
    fun findMaxAltitude(): Double {
        var maxAlt = 0.0;
        if(customGpsLocations.isNotEmpty()){
            for (i in 0 until customGpsLocations.size) {
                if(maxAlt < customGpsLocations[i].location.altitude){
                    maxAlt = customGpsLocations[i].location.altitude
                }
            }
        }

        return maxAlt
    }

    /**
     * Finding Min Altitude
     * */
    fun findMinAltitude(): Double {
        var minAlt = 0.0;
        if(customGpsLocations.isNotEmpty()){
            minAlt = customGpsLocations[0].location.altitude
            for (i in 1 until customGpsLocations.size) {
                if(customGpsLocations[i].location.altitude <= minAlt){
                    minAlt = customGpsLocations[i].location.altitude
                }
            }
        }

        return minAlt
    }

    /**
     * CustomLocation class including the speed
     * */
    inner class CustomGPSLocation {
        var current_speed: Float
            private set
        var location: Location
            private set

        constructor(current_location: Location) {
            location = current_location
            current_speed = 0f
        }

        constructor(current_location: Location, last_locLocation: Location) {
            location = current_location
            current_speed = calculateCurrentSpeed(current_location, last_locLocation)
        }

        // calculate current speed for custom gps location
        // For conversion purposes that 1 m/s is equal to 3.6 Km/h.
        private fun calculateCurrentSpeed(
            current_location: Location,
            last_Location: Location
        ): Float {
            return abs(
                last_Location.distanceTo(current_location) / (last_Location.time - current_location.time)
                        * 1000 * 3.6
            ).toFloat()
        }
    }

}