package com.griffith.gpstracker.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.griffith.gpstracker.R

class SplashScreen : AppCompatActivity() {
    private lateinit var _tvTop: TextView
    private lateinit var _tvMiddle: TextView
    private lateinit var _tvBottom: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        _tvTop = findViewById(R.id.tv_Top)
        _tvMiddle = findViewById(R.id.tv_Middle)
        _tvBottom = findViewById(R.id.tv_Bottom)

        // Creating animation
        val topAnimation = AnimationUtils.loadAnimation(this, R.anim.top_anim)
        val middleAnimation = AnimationUtils.loadAnimation(this, R.anim.middle_anim)
        val bottomAnimation = AnimationUtils.loadAnimation(this, R.anim.bottom_anim)

        // setting animation for each text view
        _tvTop.startAnimation(topAnimation)
        _tvMiddle.startAnimation(middleAnimation)
        _tvBottom.startAnimation(bottomAnimation)

        val splashScreenTimeOut = 4000
        val homeIntent = Intent(this@SplashScreen, MainActivity::class.java)

        Handler().postDelayed({
            startActivity(homeIntent)
            finish()
        }, splashScreenTimeOut.toLong())
    }
}