package com.griffith.gpstracker.activity

import android.location.Location
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.griffith.gpstracker.R
import com.griffith.gpstracker.model.LocationList
import com.griffith.gpstracker.ui.GPSGraphView
import java.util.concurrent.TimeUnit

class ReportActivity: AppCompatActivity() {
    object Constant {
        const val LOCATION_LIST_SIZE = 30
    }

    private lateinit var _customGpsGraph: GPSGraphView
    private lateinit var _averageSpeed: TextView
    private lateinit var _totalDistance: TextView
    private lateinit var _timeTaken: TextView
    private lateinit var _maxAltitude: TextView
    private lateinit var _minAltitude: TextView

    private var _elapsedTime: Int = 0
    private lateinit var _mLocationList: LocationList
    private var _locations: ArrayList<Location> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        _locations = intent.getParcelableArrayListExtra("locations")!!
        _elapsedTime = intent.getLongExtra("timeTaken", 0).toInt()

        initUI()
        initData()
    }

    // Initializing UI component
    private fun initUI(){
        _customGpsGraph = findViewById(R.id.gpsGraphCustomView)
        _averageSpeed = findViewById(R.id.tv_averageSpeed)
        _totalDistance = findViewById(R.id.tv_totalDistancec)
        _timeTaken = findViewById(R.id.tv_timeTaken)
        _maxAltitude = findViewById(R.id.tv_maxAltitude)
        _minAltitude = findViewById(R.id.tv_minAltitude)

        // calculation running time
        val minutes: Int = _elapsedTime / 1000 / 60
        val seconds = (_elapsedTime / 1000 % 60)

        _timeTaken.text = "$minutes : $seconds"
    }

    private fun initData(){
        // Creating graph
        _mLocationList = LocationList(Constant.LOCATION_LIST_SIZE)
        for (i in 0 until _locations.size) {
            _mLocationList.addLocation(_locations[i])
        }
        // set data for graph view
        _customGpsGraph.setData(_mLocationList)

        // Finding metrics
        _averageSpeed.text = String.format("%.1f", _mLocationList.averageSpeed) + " km/h"
        _totalDistance.text = String().format("%.3f", _mLocationList.calculateTotalDistance()) + " km"
        _maxAltitude.text = String.format("%.3f", _mLocationList.findMaxAltitude())
        _minAltitude.text = String.format("%.3f", _mLocationList.findMinAltitude())
    }
}