package com.griffith.gpstracker.activity

import android.Manifest
import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.griffith.gpstracker.R
import com.griffith.gpstracker.ui.SantaRunning
import com.griffith.gpstracker.util.GPXWriter
import java.io.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    private val _filePath = "GPSTracks"
    private var _gpsFile: File? = null
    private var counter: Int = 0
    private var startTime: Long = 0
    private var endTime: Long = 0

    private lateinit var _lm: LocationManager

    // Initializing UI components
    private lateinit var _lCoordinator: CoordinatorLayout
    private lateinit var fabBGLayout: View
    private lateinit var fab: FloatingActionButton
    private lateinit var fabLayout1: LinearLayout
    private lateinit var _fabStop: FloatingActionButton
    private lateinit var fabLayout2: LinearLayout
    private lateinit var _cwInstruction: CardView
    private lateinit var _fabStart: FloatingActionButton
    private lateinit var _santaRun: SantaRunning

    private var _locations: ArrayList<Location> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUi()
    }

    // Initializing related UI components
    private fun initUi(){
        _lCoordinator = findViewById(R.id.CoordinatorLayout)
        fabBGLayout = findViewById(R.id.fabBGLayout)
        fab = findViewById(R.id.fab)
        fabLayout1 = findViewById(R.id.fabLayout1)
        _fabStop = findViewById(R.id.fabStop)
        fabLayout2 = findViewById(R.id.fabLayout2)
        _cwInstruction = findViewById(R.id.cwInstruction)
        _fabStart = findViewById(R.id.fabStart)
        _santaRun = findViewById(R.id.runSanta)

        // get access to the location manager
        _lm = getSystemService(LOCATION_SERVICE) as LocationManager

        fab.setOnClickListener {
            if (View.GONE == fabBGLayout.visibility) {
                showFABMenu()
            } else {
                closeFABMenu()
            }
        }

        // FAB layout click listener
        fabBGLayout.setOnClickListener { closeFABMenu() }
        // Fab start button click listener
        _fabStart.setOnClickListener {
            startGps()
            closeFABMenu()
            showToast("Tracking has been started!")
            _santaRun.visibility = View.VISIBLE
            _cwInstruction.visibility = View.GONE
        }
        // FAB stop button click listener
        _fabStop.setOnClickListener {
            _santaRun!!.pause()
            _santaRun.visibility = View.GONE
            closeFABMenu()
            stopGps()
            showToast("Tracking has been stopped!")
        }
    }

    private fun closeFABMenu() {
        fabBGLayout.visibility = View.GONE
        fab.animate().rotation(0F)
        fabLayout1.animate().translationY(0f)
        fabLayout2.animate().translationY(0f)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {}
                override fun onAnimationEnd(animator: Animator) {
                    if (View.GONE == fabBGLayout.visibility) {
                        fabLayout1.visibility = View.GONE
                        fabLayout2.visibility = View.GONE
                    }
                }
                override fun onAnimationCancel(animator: Animator) {}
                override fun onAnimationRepeat(animator: Animator) {}
            })
    }

    private fun showFABMenu() {
        // visibility check for FAB
        fabLayout1.visibility = View.VISIBLE
        fabLayout2.visibility = View.VISIBLE
        fabBGLayout.visibility = View.VISIBLE
        fab.animate().rotationBy(180F)
        fabLayout1.animate().translationY(-resources.getDimension(R.dimen.standard_75))
        fabLayout2.animate().translationY(-resources.getDimension(R.dimen.standard_120))
    }

    private fun startGps(){
        // private function that will add a location listener that will update every 5 seconds
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            // we will request the permissions for the fine and coarse location. like setOnActivityResult
            // we have to add a request code
            // here as in larger applications there may be multiple permission requests to deal with.
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION), 1)

            // return after the call to request permissions as we don't know if the user has allowed it
            return
        }

        try {
            // Adding global location listener for trigger
            _lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0f,
            locationListener)
        }catch (ex: SecurityException){
            Log.d(TAG, "Security Exception, no location available")
        }

        startTimer()
    }

    private fun stopGps(){
        // ending tracking
        _lm.removeUpdates(locationListener)
        createGPXFile()
        goReportActivity()
    }

    //define the location listener
    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            // adding location to the list for file
            _locations.add(location)
            Toast.makeText(this@MainActivity, "Latitude: " + location.latitude.toString() + "  | Longitude: " +
                    location.longitude.toString(), Toast.LENGTH_SHORT).show()
            Log.e(
                "LOCATION",
                "Latitude " + location.latitude.toString() + "  | Longitude :" + location.longitude.toString()
            )
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    private fun goReportActivity(){
        var intent = Intent(this, ReportActivity::class.java)
        intent.putParcelableArrayListExtra("locations", _locations)
        intent.putExtra("timeTaken", endTimer())
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        // call the super class version of his first
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // check to see what request code we have
        if(requestCode == 1) {
        // if we have been denied permissions then throw a snack bar message indicating that we need them
            if(grantResults[0] == PackageManager.PERMISSION_DENIED || grantResults[1] == PackageManager.PERMISSION_DENIED)
                showToast("App will not work without location permissions")
            else
                showToast("Location permissions granted")

        }
    }

    private fun createGPXFile(){
        // create a file in internal storage and write some content to it
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        _gpsFile = File(getExternalFilesDir(_filePath), df.format(Date()) + ".xml")

        if(isExternalStorageAvailable)
            GPXWriter.writePath(_gpsFile, "GPSTracks", _locations)
        else
            showToast("Cannot access to storage!")

    }

    private fun startTimer(){
        startTime = System.currentTimeMillis()
    }

    private fun endTimer(): Long {
        endTime = System.currentTimeMillis()
        return endTime - startTime
    }

    private val isExternalStorageReadOnly: Boolean get() {
        return Environment.MEDIA_MOUNTED_READ_ONLY == Environment.getExternalStorageState()
    }

    private val isExternalStorageAvailable: Boolean get() {
        return Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()
    }

    private fun showToast(message: String){
        Snackbar.make(_lCoordinator, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onPause() {
        super.onPause()
        _lm.removeUpdates(locationListener)
        _santaRun!!.pause()
    }

    override fun onResume() {
        super.onResume()
        _santaRun!!.resume()
    }

}